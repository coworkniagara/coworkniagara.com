Oh, hello there!

Curious about the Cowork Niagara website are you? Well, first off let me start
by welcoming you to the Cowork Niagara website, Home of Niagara's Independent
Workforce.

This site is built using Middleman, Sass, Bourbon, and Neat. The file you're
reading now was served from a webserver located in Toronto, Canada, operated
by Mediadrive.ca.

Did you know our site is open source? You can view everything used to build
the site here: https://gitlab.com/coworkniagara/coworkniagara.com

If you want to meet some other people interested in websites and code, you
should come by and visit sometime!
